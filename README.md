# Aplicación TODOs "Web 1.0" #

Código de aplicación "TODOs" con tecnologías "Web 1.0":

* PHP y MySQL en el servidor
* Interfaz del lado del cliente utilizando solo HTML y CSS de Bootstrap.
* No utiliza Javascript.

Instalada y disponible en http://afrodita.inf.ucv.cl/~ifigueroa/Javascript/TODOs/Web1.0

### Instalación ###

* Utilizar archivo TablaTODO.mysql para crear la tabla en la base de datos MySQL que usted utilizará. **Ojo que se puede ocupar la misma base de datos para las versiones "1.0", "Ajax" y "Ajax-jQuery".**

* Debe copiar el archivo `db.php.template`, y ponerle nombre 'db.php', y reemplazar:

```
#!php

$servername = "YOURSERVER";
$username = "YOURUSER";
$password = "YOURPASSWORD";
$database = "YOURDATABASE";
```

por los datos y credenciales correctas a su instancia de base de datos MySQL.