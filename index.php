<?php
error_reporting(-1);
ini_set('display_errors', 'On');

require('db.php');

$datosTarea = null;

if (isset($_GET['verTODO'])) {
	$datosTarea = $mysqli->query("SELECT * FROM TODO WHERE id = {$_GET['verTODO']};") or die(mysqli_error($mysqli));
        $datosTarea = $datosTarea->fetch_assoc();
}

$idTarea = ($datosTarea['id'] ? $datosTarea['id'] : "");
$tituloTarea = ($datosTarea['titulo'] ? $datosTarea['titulo'] : "");
$descripcionTarea = ($datosTarea['descripcion'] ? $datosTarea['descripcion'] : "");
$fechaTarea = ($datosTarea['fecha'] ? date('Y-m-d', strtotime($datosTarea['fecha'])) : "");

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>TO Do List 1.0!</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

  </head>
  <body>
    <div class="container-fluid">
      <div class="row">
	<div class="col-md-4">
	  <h3 class="text-center">
	    Listado de Tareas
	  </h3>
	  <button id="newTaskBtn" type="button" class="btn btn-default" onclick="location.href='index.php'">
	    Nueva Tarea
	  </button>

	  <ul id="todoList">
	    <?php
	    if ($result = $mysqli->query("SELECT * FROM TODO")) {
		    while($row = $result->fetch_array()) {
			    echo "<li><a href='index.php?verTODO={$row['id']}'>{$row['titulo']}</a></li>";
		    }
		    
	    } else {
		    echo "FOO" . mysqli_error($mysqli);
	    }
	    ?>
	    
	    
	  </ul>

	</div>
	<div class="col-md-8">
	  <h3 class="text-center">
	    Datos de Tarea
	  </h3>
	  <form role="form" id="taskForm" action="guardarTarea.php" method="POST">
	    <div class="form-group">
	      <input type="hidden" id="taskId" name="taskId" value="<?= $idTarea; ?>"/>
	      <label for="taskTitleinput">
		Título
	      </label>
	      <input type="text" class="form-control" name="taskTitleInput" "id="taskTitleInput" placeholder="Título de la tarea..." value="<?= $tituloTarea; ?>">
	    </div>

	    <div class="form-group">
	      <label for="taskDateInput">
		Fecha
	      </label>
	      <input type="date" class="form-control" id="taskDateInput" name="taskDateInput" value="<?= $fechaTarea; ?>" /> 
	    </div>

	    <div class="form-group">
	      <label for="taskDetails">
		Detalles
	      </label>
	      <textarea name="taskDetails" form="taskForm" class="form-control" placeholder="Descripción detallada de la tarea..."><?= $descripcionTarea; ?></textarea>
	    </div>

	    <input type="submit" class="btn btn-primary" value="Guardar"/>
	    
	    <button type="reset" class="btn btn-danger">
	      Limpiar
	    </button>	    
	    <?php if (isset($datosTarea)) { ?>
	    <button id="deleteTaskBtn" type="button" class="btn btn-danger" onclick="document.getElementById('borrarForm').submit();">
	      Borrar
	    </button>
	    <?php } ?>
	  </form>

	  <?php if (isset($datosTarea)) { ?>
	  <form id="borrarForm" action="borrarTarea.php" method="POST">
	    <input type="hidden" name="taskId" value="<?= $idTarea; ?>" /> 
	  </form>
	  <?php } ?>

	</div>
      </div>
    </div>

  </body>
</html>
