<?php

error_reporting(-1);
ini_set('display_errors', 'On');

require('db.php');

if (!empty($_POST['taskTitleInput']) && !empty($_POST['taskDateInput']) && !empty($_POST['taskDetails'])) {
  
  if (!empty($_POST['taskId'])) {
    // Actualizando Tarea
    $updateQuery = <<<EOS
UPDATE TODO SET titulo = '{$_POST['taskTitleInput']}'
                , fecha = '{$_POST['taskDateInput']}'
                , descripcion = '{$_POST['taskDetails']}'
      WHERE id = {$_POST['taskId']};
EOS;
    $mysqli->query($updateQuery);

  } else {
    // Creando Tarea
    $createQuery = "INSERT INTO TODO (titulo, fecha, descripcion) VALUES ('{$_POST['taskTitleInput']}', '{$_POST['taskDateInput']}', '{$_POST['taskDetails']}');";
    $mysqli->query($createQuery);
    $_POST['taskId'] = $mysqli->insert_id;
  }

  header("Location: index.php?verTODO={$_POST['taskId']}");
  die();  
  
} else {
  echo "EMPTY CAMPOS";
}

?>
